const createTable = () => {
    let rowNumber = parseFloat(document.getElementById('rowNumber').value);
    let columnNumber = parseFloat(document.getElementById('columnNumber').value);

    if (parseInt(String(rowNumber)) !== rowNumber || parseInt(String(columnNumber)) !== columnNumber) {
        alert('Can\'t create table with float dimensions');
        return;
    }


    let table = document.getElementById('table');


    if (table === null){
        table = document.createElement('table');
        table.id = 'table';
    }
    else if(table.rows.length === rowNumber && table.rows[0].cells.length === columnNumber)
        return;
    else
        table.innerHTML = '';


    for (let row = 1; row <= rowNumber; row++){
        let rowElement = document.createElement('tr');
        rowElement.id = `row${row}`;
        rowElement.addEventListener("click", cellClick, true);

        for (let column = 1; column <=columnNumber; column++){
            let columnElement = document.createElement('td')
            columnElement.id = `cell${row}/${column}`;

            rowElement.appendChild(columnElement);
        }

        table.appendChild(rowElement);
    }


    document.body.appendChild(table);
};

const putNumbers = () => {
    let table = document.getElementById('table');

    for (let row of table.rows)
        for (let cell of row.cells)
            cell.textContent = cell.id.substr(4).split('/').join('');
};

const fillCell = (cell) => {
    if (cell.style.backgroundColor !== ''){
        cell.style.backgroundColor = '';
        cell.style.color = 'black';
    }
    else{
        cell.style.backgroundColor =
            `rgb(
                    ${Math.random()*256},
                    ${Math.random()*256},
                    ${Math.random()*256}
                    )`;

        cell.style.color = 'white';
    }
}

const borderClick = (cell, x, y, left, right, top, bot) => {
    let row = cell.id.replace('cell', '').split('/')[0];
    let col = cell.id.replace('cell', '').split('/')[1];

    if (x < left){
        let leftNeighbour = document.getElementById('cell' + row + '/' + (col - 1));

        colorExchange(cell, leftNeighbour);
    }
    else if (x > right) {
        fillCell(cell);
    }

    if (y < top) {
        let topNeighbour = document.getElementById('cell' + (row - 1) + '/' + col);

        colorExchange(cell, topNeighbour);
    }
    else if (y > bot) {
        fillCell(cell);
    }
}

const colorExchange = (cell, neighbour) => {
    let tempColor;

    if (neighbour === null)
        fillCell(cell)
    else {
        tempColor = cell.style.backgroundColor;
        cell.style.backgroundColor = neighbour.style.backgroundColor;
        neighbour.style.backgroundColor = tempColor;

        tempColor = cell.style.color;
        cell.style.color = neighbour.style.color;
        neighbour.style.color = tempColor;
    }
}

const cellClick = () => {

        if (event.target.nodeName === 'TD'){

            let cell = event.target;

            let cell_style = getComputedStyle(cell);

            let left = parseInt(cell_style.borderLeftWidth.replace('px', ''));
            let right = parseInt(cell_style.borderRightWidth.replace('px', '')) + parseInt(cell_style.width.replace('px', ''));
            let top = parseInt(cell_style.borderTopWidth.replace('px', ''));
            let bot = parseInt(cell_style.borderBottomWidth.replace('px', '')) + parseInt(cell_style.height.replace('px', ''));

            let x = event.offsetX, y = event.offsetY;

            if (x < left || x > right || y < top || y > bot)
                borderClick(cell, x, y, left, right, top, bot);
            else
                fillCell(cell);
        }

}



